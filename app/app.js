'use strict';
angular.module('templates', []);

angular
    .module('MyApp', [
        'Articles',
        'Authentication',
        'ngRoute',
        'ngAnimate',
        'ngCookies',
        'infinite-scroll',
        'templates'
    ])
    .config(appConfig)
    .run(appRun);

/**
 * Configuracion de rutas
 */
function appConfig($routeProvider) {
    $routeProvider
        .when("/", {
            templateUrl: "modules/articles/views/articles.html",
            controller: 'ArticleController',
            controllerAs: 'articleCtrl',
            data: {
                animation: 'first',
                private: false
            }
        })
        .when("/posts/:PK", {
            templateUrl: "modules/articles/views/posts.html",
            controller: 'PostController',
            controllerAs: 'postCtrl',
            data: {
                animation: 'second',
                private: false
            }
        })
        .when("/upload/:PK?", {
            templateUrl: "modules/articles/views/upload.html",
            controller: 'UploadController',
            controllerAs: 'uploadCtrl',
            data: {
                animation: 'third',
                private: true
            }
        })
        .when("/login", {
            templateUrl: "modules/authentication/views/login.html",
            controller: 'LoginController',
            controllerAs: 'loginCtrl',
            data: {
                animation: 'third',
                private: false
            }
        })
        .when("/signup", {
            templateUrl: "modules/authentication/views/signup.html",
            controller: 'SignupController',
            controllerAs: 'signupCtrl',
            data: {
                animation: 'second',
                private: false
            }
        })
        .when("/profile/:USER", {
            templateUrl: "modules/authentication/views/profile.html",
            controller: 'ProfileController',
            controllerAs: 'profileCtrl',
            data: {
                animation: 'third',
                private: false
            }
        })
        .when("/update/profile", {
            templateUrl: "modules/authentication/views/update_profile.html",
            controller: 'UpdateProfileController',
            controllerAs: 'uProCtrl',
            data: {
                animation: 'third',
                private: true
            }
        })
        .otherwise({
            redirectTo: '/'
        });
}

/**
 * Configuracion inicial
 */
function appRun($rootScope, $location, $cookieStore, $http, $route, AuthenticationService) {
    $rootScope.backUrl = false; // Back URL opcional
    $rootScope.busy = false; // Para mostrar la maskara de cargando..

    /**
     * Logout
     */
    $rootScope.logout = function () {
        AuthenticationService.ClearCredentials();
        // Recargamos la ruta actual, para que el control de acceso se encarge de redirecciónar si hace falta
        $rootScope.backUrl = false;
        $route.reload();
    };

    // Si hay cookies, las cojemos, para disponer de ellas en el rootScope
    $rootScope.globals = $cookieStore.get('globals') || {};

    /**
     * Controlar el cambio de rutas
     */
    $rootScope.$on('$routeChangeStart', function (event, currRoute, prevRoute) {
        if (!currRoute.data) {
            return;
        }

        // Animacion
        $rootScope.animation = currRoute.data.animation;

        // Contorl de acceso
        if (currRoute.data.private && !$rootScope.globals.token) {
            $location.path('/login');
        }
    });
}