/**
 Copyright 2017 Jonathan Millan.

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 **/

$(function () {
    'use strict';

    // Mostrar menu lateral
    $('#toggle_menu').click(function () {
        $('#mask').show();
        $('#menu').addClass('active');
    });

    // Ocultar la maskara al tocarla o escojer opcion
    $('#mask, #menu a').click(function () {
        $('#mask').hide();
        $('#menu').removeClass('active');
    });

    // Mostrar buscador
    $('#do_search').click(function () {
        // TODO: No ocultar al clickar si esta abierto el buscador
        toggleSearch();
    });

    // Ocultar buscador
    $('#cancel_search').click(function () {
        toggleSearch();
    });

    function toggleSearch() {
        $('.main_feeds').toggleClass('hide'); // Ocultar Opciones feeds
        $('#do_search').closest('.searcher').toggleClass('active'); // Activar el buscador
        $('#search').focus(); // Marcar el input
    }
});