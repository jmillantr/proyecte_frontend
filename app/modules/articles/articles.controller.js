angular.module('Articles', []);

angular
    .module('Articles')
    .controller('ArticleController', ArticleController)
    .controller('PostController', PostController)
    .controller('UploadController', UploadController);

/**
 * Controlador de los Articulos
 * @param $scope
 * @param $http
 * @param $rootScope
 * @constructor
 */
function ArticleController($scope, $http, $rootScope) {
    $rootScope.backUrl = false;
    this.articles = [];
    this.dataLoading = false;
    this.source = 'popular';
    this.url = 'https://api.memestring.com/articles/?format=json&source=' + this.source;

    // Asignar el souce de datos
    this.setSource = function (setSource) {
        this.source = setSource;
        this.url = 'https://api.memestring.com/articles/?format=json&source=' + setSource;
        // TODO: Se han de reiniciar los articulos visibles solo si cambia el source
        // TODO: Asegurar que se reinician los articulos
        this.articles = [];
    };

    // Comprobar el source de datos
    this.isSource = function (checkSource) {
        return this.source === checkSource;
    };

    // Cargar nuevos articulos de la API
    this.loadMore = function () {
        // No sobrecargar el load
        if (this.dataLoading) return;

        // Si no tenemos URL salimos
        if (this.url === null) return;

        this.dataLoading = true;

        $http.get(this.url).then(
            // successCallback
            function (response) {
                // Cojemos los articulos
                var new_articles = response.data.results;

                for (var i = 0; i < new_articles.length; i++) {
                    this.articles.push(new_articles[i]);
                }

                this.url = response.data.next;
                this.dataLoading = false;
            }.bind(this),
            // errorCallback
            function (response) {
                console.log('Error');
            });
    }
}


/**
 * Controlador de los Posts
 * @param $scope
 * @param $http
 * @param $routeParams
 * @param $rootScope
 * @constructor
 */
function PostController($scope, $http, $routeParams, $rootScope) {
    $rootScope.backUrl = '#!/';
    this.article_pk = $routeParams.PK;
    this.posts = [];
    this.dataLoading = false;
    this.url = 'https://api.memestring.com/articles/' + this.article_pk + '/posts/?format=json';

    // Cargar nuevos articulos de la API
    // TODO: Mirar de reutilizar el metodo
    this.loadMore = function () {
        // No sobrecargar el load
        if (this.dataLoading) return;

        // Si no tenemos URL salimos
        if (this.url === null) return;

        this.dataLoading = true;

        $http.get(this.url).then(
            // successCallback
            function (response) {
                // Cojemos los posts
                var new_posts = response.data.results;

                for (var i = 0; i < new_posts.length; i++) {
                    this.posts.push(new_posts[i]);
                }

                this.url = response.data.next;
                this.dataLoading = false;
            }.bind(this),
            // errorCallback
            function (response) {
                console.log('Error');
            });
    }
}


/**
 * Controlador de Upload
 * @param $scope
 * @param $http
 * @param $routeParams
 * @param $rootScope
 * @param $location
 * @constructor
 */
function UploadController($scope, $http, $routeParams, $rootScope, $location) {
    $rootScope.backUrl = '#!/';
    $scope.errors = [];
    this.article_pk = $routeParams.PK;
    this.url = 'https://api.memestring.com/articles/';

    // Si subimos a un articulo concreto, ajustamos la url
    if (this.article_pk) {
        this.url += this.article_pk + '/posts/';
        $rootScope.backUrl = '#!posts/' + this.article_pk;
    }

    this.upload = function () {
        // No sobrecargar
        if ($rootScope.busy) return;
        $rootScope.busy = true;

        // Serializamos el formulario
        var fd = new FormData();
        fd.append('media', $scope.fileArray[0]);
        fd.append('caption', $scope.caption);

        $http.post(this.url, fd, {
            // this cancels AngularJS normal serialization of request
            transformRequest: angular.identity,
            // this lets browser set `Content-Type: multipart/form-data`
            // header and proper data boundary
            headers: {'Content-Type': undefined}
        }).then(
            // successCallback
            function (response) {
                $rootScope.busy = false;
                $location.path('/posts/' + response.data.article_pk);
            },
            // errorCallback
            function (response) {
                for (var key in response.data) {
                    for (var i = 0; i < response.data[key].length; i++) {
                        $scope.errors.push(response.data[key][i]);
                    }
                }

                $rootScope.busy = false;
            });
    };
}