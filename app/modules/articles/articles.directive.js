angular
    .module('Articles')
    .directive('filesInput', filesInput);

/**
 * Directiva de input para ficheros
 * @returns {{require: string, link: postLink}}
 */
function filesInput() {
    return {
        require: "ngModel",
        link: function postLink(scope, elem, attrs, ngModel) {
            elem.on("change", function (e) {
                var files = elem[0].files;
                ngModel.$setViewValue(files);
            })
        }
    }
}