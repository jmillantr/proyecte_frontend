angular
    .module('Authentication')
    .config(authenticationConfig);

/**
 * $http Interceptor, para capturar todas las transacciónes http
 */
function authenticationConfig($provide, $httpProvider) {
    /**
     * Podemos capturar los siguientes eventos
     * request, requestError, response, responseError
     */
    $provide.factory('authHttpInterceptor', function ($q, $location, $rootScope) {
        return {
            // request Interceptor
            'request': function (config) {
                // Si disponemos de token, lo asignamos
                if ($rootScope.globals.token) {
                    // TODO: Podemos reiniciar el token cada x tiempo, si el usuario no desconecta
                    config.headers.Authorization = 'JWT ' + $rootScope.globals.token;
                }

                return config;
            },

            // responseError Interceptor
            'responseError': function (rejection) {
                // Si el codigo es 401 o 403, es que el token no es valido, por lo tanto lo retiramos
                if (rejection.status === 401 || rejection.status === 403) {
                    $rootScope.logout();
                }

                return $q.reject(rejection);
            }
        };
    });

    /**
     * Asignamos el Interceptor al $httpProvider
     */
    $httpProvider.interceptors.push('authHttpInterceptor');
}