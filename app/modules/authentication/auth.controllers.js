angular.module('Authentication', []);

angular
    .module('Authentication')
    .controller('LoginController', LoginController)
    .controller('SignupController', SignupController)
    .controller('ProfileController', ProfileController)
    .controller('UpdateProfileController', UpdateProfileController);

/**
 * Controlador del Login
 */
function LoginController($scope, $rootScope, $location, AuthenticationService) {
    $rootScope.back = false;
    $scope.errors = [];

    /**
     * Login
     */
    $scope.login = function () {
        $rootScope.busy = true;
        AuthenticationService.Login($scope.username, $scope.password, function (response) {
            if (response.status === 200) {
                AuthenticationService.SetCredentials($scope.username, response.data.token);
                $rootScope.busy = false;
                $location.path('/');
            } else {
                $scope.password = null;
                $scope.errors.push(response.message);
                $rootScope.busy = false;
            }
        });
    };
}

/**
 * Controlador de Signup
 */
function SignupController($scope, $http, $routeParams, $rootScope, $location) {
    $rootScope.back = false;
    $scope.errors = [];

    /**
     * Singup
     */
    $scope.signup = function () {
        $rootScope.busy = true;

        $http.post('https://api.memestring.com/users/',
            {
                username: $scope.username,
                email: $scope.email,
                password: $scope.password,
                confirm_password: $scope.confirm_password,
                // TODO: Buscar el modo de que no se requiera el perfil
                profile: {
                    avatar: null
                }

            }).then(
            // successCallback
            function (response) {
                $rootScope.busy = false;
                $location.path('/login');
            },
            // errorCallback
            function (response) {
                for (var key in response.data) {
                    for (var i = 0; i < response.data[key].length; i++) {
                        $scope.errors.push(response.data[key][i]);
                    }
                }

                $rootScope.busy = false;
            });
    };
}

/**
 * Controlador de Profile
 */
function ProfileController($scope, $http, $routeParams, $rootScope) {
    $rootScope.back = false;

    this.url = 'https://api.memestring.com/users/' + $routeParams.USER + '/?format=json';
    this.user = {};
    this.isSameUser = $routeParams.USER === $rootScope.globals.username;

    $http.get(this.url).then(
        // successCallback
        function (response) {
            this.user = response.data;
        }.bind(this),
        // errorCallback
        function (response) {
            console.log('Error');
        });
}

/**
 * Controlador de Profile
 */
function UpdateProfileController($scope, $http, $routeParams, $rootScope, $location) {
    $rootScope.backUrl = '#!/profile/' + $rootScope.globals.username;
    // TODO: Poner errores para cada formulario por separado
    $scope.errors = [];
    this.url = 'https://api.memestring.com/users/' + $rootScope.globals.username;
    this.user = {};

    /**
     * Load current data
     */
    $http.get(this.url).then(
        // successCallback
        function (response) {
            this.user = response.data;
        }.bind(this),
        // errorCallback
        function (response) {
            console.log('Error');
        });

    /**
     * Cambiar Imagen de perfil
     */
    this.changeProfilePicture = function () {
        // No sobrecargar
        if ($rootScope.busy) return;
        $rootScope.busy = true;

        // Serializamos el formulario
        var fd = new FormData();
        fd.append('avatar', $scope.fileArray[0]);

        $http.put(this.url + '/change_profile/', fd, {
            // this cancels AngularJS normal serialization of request
            transformRequest: angular.identity,
            // this lets browser set `Content-Type: multipart/form-data`
            // header and proper data boundary
            headers: {'Content-Type': undefined}
        }).then(
            // successCallback
            function (response) {
                $rootScope.busy = false;
                $location.path('/profile/' + $rootScope.globals.username);
            },
            // errorCallback
            function (response) {
                for (var key in response.data) {
                    for (var i = 0; i < response.data[key].length; i++) {
                        $scope.errors.push(response.data[key][i]);
                    }
                }

                $rootScope.busy = false;
            });
    };

    /**
     * Cambiar URLs Social
     */
    this.changeSocia = function () {
        // No sobrecargar
        if ($rootScope.busy) return;
        $rootScope.busy = true;

        $http.put(this.url + '/change_social/', {
            facebook: $scope.facebook,
            twitter: $scope.twitter,
            pinterest: $scope.pinterest,
            dribbble: $scope.dribbble
        }).then(
            // successCallback
            function (response) {
                $rootScope.busy = false;
                $location.path('/profile/' + $rootScope.globals.username);
            },
            // errorCallback
            function (response) {
                for (var key in response.data) {
                    for (var i = 0; i < response.data[key].length; i++) {
                        $scope.errors.push(response.data[key][i]);
                    }
                }

                $rootScope.busy = false;
            });
    };

    /**
     * Cambiar Contraseña
     */
    this.changePassword = function () {
        // No sobrecargar
        if ($rootScope.busy) return;
        $rootScope.busy = true;

        $http.put(this.url + '/change_password/', {
            old_password: $scope.old_password,
            new_password: $scope.new_password
        }).then(
            // successCallback
            function (response) {
                $rootScope.busy = false;
                $location.path('/profile/' + $rootScope.globals.username);
            },
            // errorCallback
            function (response) {
                for (var key in response.data) {
                    for (var i = 0; i < response.data[key].length; i++) {
                        $scope.errors.push(response.data[key][i]);
                    }
                }

                $rootScope.busy = false;
            });
    };
}
