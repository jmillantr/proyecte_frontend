angular
    .module('Authentication')
    .factory('AuthenticationService', AuthenticationService);

/**
 * Servicio para asignar y desasignar credenciales
 */
function AuthenticationService($http, $cookieStore, $rootScope) {
    var service = {};

    /**
     * Logea en la API
     * @param username
     * @param password
     * @param callback
     * @constructor
     */
    service.Login = function (username, password, callback) {

        // Mandamos las credenciales para solicitar el Token
        $http.post('https://api.memestring.com/auth/', {username: username, password: password}).then(
            // successCallback
            function (response) {
                callback(response);
            },
            // errorCallback
            function (response) {
                response.message = 'Username or password is incorrect';
                callback(response);
            });
    };

    /**
     * Asigna el token en los Headers
     * @param username
     * @param token
     * @constructor
     */
    service.SetCredentials = function (username, token) {

        // Creamos las globales para tener un acceso mas directo
        $rootScope.globals = {
            username: username,
            token: token
        };

        // Guardar el token en las cookies para futuras entradas
        $cookieStore.put('globals', $rootScope.globals);
    };

    /**
     * Borra todas las credenciales y las cookies
     * @constructor
     */
    service.ClearCredentials = function () {
        $rootScope.globals = {};
        $cookieStore.remove('globals');
    };

    return service;
}
