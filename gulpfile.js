var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var ngAnnotate = require('gulp-ng-annotate');
var angularOrder = require('gulp-angular-order');
var imagemin = require('gulp-imagemin');
var addStream = require('add-stream');
var angularTemplateCache = require('gulp-angular-templatecache');
var cleanCSS = require('gulp-clean-css');
var htmlmin = require('gulp-htmlmin');

// Rutas
var paths = {
    dest: '../www/',
    html: ['app/index.html'],
    css: ['app/css/*.css'],
    libs: ['app/css/fonts/*'],
    views: ['app/**/*.html'],
    images: ['app/images/**/*.png', 'app/images/**/*.jpg', 'app/images/**/*.svg']
};

/**
 * Munifyficar y copiar todos los JS
 */
gulp.task('deploy', function () {
    return gulp.src('app/**/*.js')
        .pipe(addStream.obj(prepareTemplates()))
        .pipe(angularOrder())
        .pipe(concat('app.js'))
        .pipe(ngAnnotate({add: true}))
        .pipe(uglify())
        .pipe(gulp.dest(paths.dest));
});

/**
 * Minificar las imagenes
 */
gulp.task('images', function () {
    gulp.src(paths.images)
        .pipe(imagemin())
        .pipe(gulp.dest(paths.dest + 'images/'));
});

/**
 * Delete the dest directory
 */
gulp.task('clean', function () {
    return gulp.src(paths.dest)
        .pipe(clean());
});

/**
 * Mover HTMLs
 */
gulp.task('minify-html', function () {
    // Copy html
    gulp.src(paths.html)
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(gulp.dest(paths.dest));
});

/**
 * Mover CSS
 */
gulp.task('minify-css', function () {
    // Copy css
    gulp.src(paths.css)
        .pipe(concat('master.css'))
        .pipe(cleanCSS())
        .pipe(gulp.dest(paths.dest + '/css'));

    // Copy libs
    gulp.src(paths.libs)
        .pipe(gulp.dest(paths.dest + '/css/fonts'));
});

/**
 * Cojer todas las plantillas HTML y unirlas
 * @returns {*}
 */
function prepareTemplates() {
    return gulp.src(paths.views)
        .pipe(angularTemplateCache());
}

gulp.task('default', ['deploy', 'minify-html', 'minify-css', 'images']);